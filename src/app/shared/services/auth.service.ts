import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { tap, catchError } from 'rxjs/operators';
import { HttpErrorResponse } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { CookieService } from 'ngx-cookie';
import { User } from '../classes/user';
import { Output, EventEmitter } from '@angular/core';

const requestOptions = {
  withCredentials: true,
};

@Injectable()
export class AuthService {
  private url = `${environment.apiBaseUrl}/user`;
  private cookieKey = 'currentUser';

  constructor(private http: HttpClient, private cookieService: CookieService) {}

  login(email: string, password: string): Observable<Boolean | User> {
    console.log('auth.service login');

    const loginInfo = { email: email, password: password };

    return this.http
      .put<User>(`${this.url}/login`, loginInfo, requestOptions)
      .pipe(
        tap((user: User) => {
          if (user) {
            console.log('logged in');
            this.setUser(user);
            return of(true);
          }

          console.log('not logged in');
          this.clearUser();
          return of(false);
        }),
        catchError(error => {
          console.log('login error', error);
          return of(false);
        })
      );
  }

  signup(email: string, password: string): Observable<Boolean | User> {
    const loginInfo = { email: email, password: password };
    return this.http
      .post<User>(this.url, loginInfo, requestOptions)
      .pipe(
        tap((user: User) => {
          if (user) {
            this.setUser(user);
            return of(true);
          }

          this.clearUser();
          return of(false);
        }),
        catchError(error => {
          console.log('signup error', error);
          return of(false);
        })
      );
  }

  isAuthenticated(): Observable<Boolean | User> {
     return this.http
     .get<User>(`${this.url}/identity`, requestOptions)
     .pipe(
         tap((user: User) => {
             if (user) {
                 console.log('logged in');
                 this.setUser(user);
                 return of(true);
             }

             console.log('not logged in');
             this.clearUser();
             return of(false);
         }),
         catchError((error: HttpErrorResponse) => {
             if (error.status !== 403) {
                 console.log('isAuthenticated error', error);
             }
             console.log('not logged in', error);
             return of(false);
         }),
     );
   }

  getUser(): User {
    return <User>this.cookieService.getObject(this.cookieKey)
  }

  setUser(value: User): void {
    this.cookieService.putObject(this.cookieKey, value);
    this.getLoggedInUser.emit(value);
  }

  clearUser() : void {
    this.cookieService.remove(this.cookieKey);
    this.getLoggedInUser.emit(null);
  }

  logout(): Observable<Boolean | Response> {
    return this.http
    .get<User>(`${this.url}/logout`, requestOptions)
    .pipe(
        tap((res: Response) => {
            this.clearUser();
            if (res.ok) {
                return of(true);
            }

            return of(false);
       }),
        catchError((error: HttpErrorResponse) => {
            this.clearUser();
            return of(false);
        })
    );
  }

  @Output() getLoggedInUser: EventEmitter<User> = new EventEmitter<User>();
}
