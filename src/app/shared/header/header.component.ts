import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { User } from '../classes/user';
import { OnDestroy } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  public isCollapsed = true;
  public loggedInUser: User;
  public subscription: any;

  constructor(private authService: AuthService, private router: Router) { }

  ngOnInit() {
    this.loggedInUser = this.authService.getUser();

    this.authService.getLoggedInUser.subscribe(user => {
      this.loggedInUser = user;
      console.log('user changed through emit', this.loggedInUser);
    });
  }

  toggleMenu() {
    this.isCollapsed = !this.isCollapsed;
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  logout() {
    this.authService.logout().subscribe(() => {
        this.router.navigate(['/login']);
    });
  }

}
